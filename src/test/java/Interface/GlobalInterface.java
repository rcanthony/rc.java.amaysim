package Interface;

public interface GlobalInterface {
	void verifyElementsAreVisible(String[] arrayLocator);
	void verifyElementIsVisible(String locator);
	void verifyElementIsNotVisible(String locator);
	void clickElement(String locator);
	void inputText(String locator,String textInput);
	void verifyElementText(String locator,String text);
	void clearText(String locator);
	void scrollDown(int pixel);
	void verifyElementContainsText(String locator, String text);
}
