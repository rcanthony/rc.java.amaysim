package TestSuite;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeTest;

import Page.*;
import Interface.*;
import Resource.*;

public class GlobalTest extends WebDriverController{
	Constants c = new Constants();
	Locators l = new Locators();
    protected GlobalInterface GlobalAction;
    protected Login LoginPage;
    protected CallForwarding CallForwardingPage;
    
	@BeforeTest
	public void SuiteSetup() {
		ChromeBrowser();
		GlobalAction = new GlobalAction(driver());
		PageFactory.initElements(driver(), GlobalAction);
		LoginPage = new LoginPage(driver());
		PageFactory.initElements(driver(), LoginPage);
		CallForwardingPage = new CallForwardingPage(driver());
		PageFactory.initElements(driver(), CallForwardingPage);
		driver().get(c.ProjectURL);
	}
	@AfterTest
	public void SuiteTearDown() {
		quitDriver();
	}	
	@BeforeGroups("callForwarding")
	public void TestSetup() {	
		LoginPage.login();
		CallForwardingPage.CallForwardingSetup();	
	}
	
	@AfterGroups("callForwarding")
	public void TestTeardown() {
		if(driver().findElement(By.xpath(l.CallForwading_CloseMessage)).isDisplayed()) {
			GlobalAction.clickElement(l.CallForwading_CloseMessage);
		}
		GlobalAction.clickElement(l.CallForwading_Edit);
		GlobalAction.clickElement(l.Confirm);
		GlobalAction.clearText(l.ForwardCall_TextField);
		GlobalAction.inputText(l.ForwardCall_TextField, c.TestAustralianNumber[0]);
		GlobalAction.verifyElementIsNotVisible(l.LoadingSpinner);
		GlobalAction.clickElement(l.Save);
		GlobalAction.verifyElementIsNotVisible(l.LoadingSpinner);
		GlobalAction.clickElement(l.CallForwading_CloseMessage);
	}
}
