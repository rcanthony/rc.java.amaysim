package Resource;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriverController {
	private ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>();
	
	public void ChromeBrowser() {
		final String dir = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", dir + "\\src\\test\\java\\Resource\\Driver\\chromedriver.exe");
		  try {
	          webDriver.set(new ChromeDriver());
	        } catch (WebDriverException e) {
	          e.printStackTrace();
	        }
		  driver().manage().window().maximize();
	}
	
	
	public WebDriver driver() {
		return webDriver.get();
	}
	
	public void quitDriver() {
		driver().quit();
	}
	
}
