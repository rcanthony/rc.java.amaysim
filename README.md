## Setup for windows
	Download Chromedriver on https://chromedriver.storage.googleapis.com/index.html?path=74.0.3729.6/
	Download And Install Java
	Download And Install Maven
	Set Environmental Variable for both Java and Maven


** Check if the variables are properly set **  
	on bash/terminal, try  
		java -version  
		mvn -version  

	also take note that chromedriver version should be the same with the version of chrome
	
** To run or execute a test **  
	on bash/terminal. 
	navigate to the project folder. 
	Try `dir` or `ls`. Make sure that the pom.xml and testng.xml is in the list. 
	Then simply enter `mvn clean test` to run the test.