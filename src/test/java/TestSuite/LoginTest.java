package TestSuite;

import org.testng.annotations.Test;

public class LoginTest extends GlobalTest{
@Test
public void verifyValidLogin() throws InterruptedException {
	
	LoginPage.login();
	GlobalAction.verifyElementContainsText(l.WelcomeMessage, c.User);
}
}
