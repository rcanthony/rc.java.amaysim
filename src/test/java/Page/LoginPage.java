package Page;

import org.openqa.selenium.WebDriver;

import Interface.Login;
import Resource.Constants;
import Resource.Locators;

public class LoginPage extends GlobalAction implements Login{
	Locators l = new Locators();
	Constants c = new Constants();
    public LoginPage (WebDriver driver) {
    	super(driver); 
    	}
	
	public void login()
	{
		String[] loginLocator={l.UsernameField,l.PasswordField,l.LoginPage_LoginButton};
		String[] loginVariable={c.UserNumber,c.UserPassword};
		clickElement(l.HomePage_LoginButton);
		for(int i=0; i<loginLocator.length; i++) 
		{
			if(i<loginLocator.length-1) {
				inputText(loginLocator[i],loginVariable[i]);
			}
			else {
				clickElement(loginLocator[i]);
			}
		}
	}
}
