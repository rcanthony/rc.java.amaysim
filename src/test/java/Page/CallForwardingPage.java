package Page;

import org.openqa.selenium.WebDriver;

import Interface.CallForwarding;
import Resource.Locators;

public class CallForwardingPage extends GlobalAction implements CallForwarding{
	Locators l = new Locators();
	
    public CallForwardingPage (WebDriver driver) {
    	super(driver); 
    	}
    
	public void CallForwardingSetup() {
		scrollDown(7000);
		clickElement(l.ASYOUGO_ManagePlan);
		clickElement(l.Settings);
		verifyElementIsNotVisible(l.LoadingSpinner);
		verifyElementIsNotVisible(l.LoadingSpinner);
		clickElement(l.CallForwading_Edit);
		clickElement(l.Confirm);
		verifyElementIsNotVisible(l.LoadingSpinner);
	}
	

}
