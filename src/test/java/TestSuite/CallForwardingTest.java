package TestSuite;

import org.testng.annotations.*;


public class CallForwardingTest extends GlobalTest{
	
	@Test(groups = "callForwarding") 	//Alert message should prompt on successful update of settings
	public void SettingsUpdateAlert() 
	{
		GlobalAction.clearText(l.ForwardCall_TextField);
		GlobalAction.inputText(l.ForwardCall_TextField, c.TestAustralianNumber[1]);
		GlobalAction.clickElement(l.Save);
		GlobalAction.verifyElementIsNotVisible(l.LoadingSpinner);
		GlobalAction.verifyElementText(l.SuccessfulUpdateAlert, c.SuccessfulUpdateMessage);
	}
	
}
