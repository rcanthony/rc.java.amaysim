package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Interface.GlobalInterface;

public class GlobalAction implements GlobalInterface {
	public WebDriver driver;
	public WebDriverWait wait;
	public JavascriptExecutor js;

	public GlobalAction(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 40);
		js= (JavascriptExecutor) driver;
	}

	public void verifyElementsAreVisible(String[] arrayLocator) {
		for (int i = 0; i < arrayLocator.length; i++) {
			WebElement elementLocator = wait
					.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(arrayLocator[i])));
			Assert.assertTrue(elementLocator.isDisplayed());
		}
	}

	public void verifyElementIsVisible(String locator) {
		WebElement elementLocator = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
		Assert.assertTrue(elementLocator.isDisplayed());
	}

	public void verifyElementIsNotVisible(String locator) {
		boolean ctr = true;
		do {
			if (driver.findElement(By.xpath(locator)).isDisplayed()) {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else {
				ctr = false;
			}
		} while (ctr);
		Assert.assertFalse(driver.findElement(By.xpath(locator)).isDisplayed());
	}

	public void clickElement(String locator) {
		WebElement elementLocator = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
		elementLocator.click();
	}

	public void inputText(String locator, String textInput) {
		WebElement elementLocator = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
		elementLocator.sendKeys(textInput);
	}

	public void verifyElementText(String locator, String text) {
		WebElement elementLocator = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
		Assert.assertEquals(elementLocator.getText(), text);
	}

	public void clearText(String locator) {
		WebElement elementLocator = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
		elementLocator.clear();
	}

	public void scrollDown(int pixel) {
		js.executeScript("scrollBy(0, " + pixel + ")");
	}
	
	public void verifyElementContainsText(String locator, String text) {
		boolean pass = false;
		WebElement elementLocator = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
		String elementText = elementLocator.getText();
		if(elementText.contains(text)) {
			pass = true;
		}
		Assert.assertTrue(pass);

	}

}
