package Resource;

public class Locators {
	public final String HomePage_LoginButton = "//span[text()='Login']";
	public final String UsernameField	= "//input[@id='username']";
	public final String PasswordField = "//input[@id='password']";
	public final String LoginPage_LoginButton = "//button[@name='button']";
	public final String CallForwading_Edit = "//a[@id='edit_settings_call_forwarding']";
	public final String Save ="//input[@type='submit' and @value='Save']";
	public final String LoadingSpinner ="//div[@class='loading-spinner']";
	public final String CallForwading_CloseMessage = "(//a[@class='close-reveal-modal'])[6]";
	public final String Confirm = "//a[text()='Confirm']";
	public final String CallForwarding_RadioYes = "//span[text()='Yes']";
	public final String ForwardCall_TextField = "//input[@id='my_amaysim2_setting_call_divert_number']";
	public final String SuccessfulUpdateAlert = "(//div/p[1])[3]";
	public final String ASYOUGO_ManagePlan = "//a[text()='Manage plan']";
	public final String Settings ="//span[text()='Settings']";
	public final String WelcomeMessage ="//span[@id='WelcomeMessage']";
}
